# -*- coding: utf-8 -*-
import os
import cx_Oracle
import data_transformation_utils

connection = cx_Oracle.connect(user="DATA_MANAGER", password="FC15903117947bD6", dsn="10.105.1.112/CSPRDDB")


def generate_students_filter(student_list, discontinued=False):
    column = "EMPLID"
    if discontinued:
        column = "LQBH"
    if len(student_list) > 0:
        if len(student_list) == 1:
            return "AND {} = '{}'".format(column, student_list[0])
        return "AND {} IN ('{}')".format(column, "','".join(student_list))
    return ""


def generate_fees_filter(fee_list):
    fee_descr_list = []
    if len(fee_list) > 0:
        if len(fee_list) == 1:
            return "AND DESCR = '{}'".format(data_transformation_utils.get_fee_description(fee_list[0]))
        for fee in fee_list:
            fee_descr_list.append(data_transformation_utils.get_fee_description(fee))
        return "AND DESCR IN ('{}')".format("','".join(fee_descr_list))
    return ""


def create_directory_if_not_exists(file_path):
    dir_path = os.path.dirname(file_path)
    if not os.path.exists(dir_path):
        os.makedirs(dir_path)


def write_file_content(file_path, mode, encoding, file_content):
    create_directory_if_not_exists(file_path)
    f = open(file_path, mode, encoding=encoding)
    try:
        f.write(file_content)
    except Exception as err:
        print(err)
    finally:
        f.close()


def create_academic_org_data_file(academic_program=""):
    if academic_program == "":
        content = "29,浙江大学国际联合学院（海宁国际校区）"
        write_file_content(f"data_files\\org.txt", "w+", "GBK", content)


def create_academic_plan_data_file(academic_program="", academic_plan=""):
    if academic_program == "" and academic_plan == "":
        content = "29,HNXQ,海宁校区专业"
        write_file_content(f"data_files\\plan.txt", "w+", "GBK", content)


def create_class_data_file(admit_term, academic_career, academic_program=""):
    programs = ["UE001", "UC001"]
    if academic_program != "":
        programs.append(academic_program)
    class_items = []
    for program in programs:
        content = ",".join([data_transformation_utils.getZYDM(academic_career),
                            data_transformation_utils.get_class_id(admit_term, academic_career, program),
                            data_transformation_utils.get_class_descr(admit_term, academic_career, program),
                            data_transformation_utils.get_year(admit_term),
                            data_transformation_utils.getJC(admit_term)])
        class_items.append(content)
    write_file_content(f"data_files\\class\\{admit_term}\\class.txt", "w+", "GBK", "\n".join(class_items))


def create_student_data_file(academic_career, admit_term, fee_term, discontinued, student_list):
    try:
        if academic_career is None or admit_term is None:
            raise Exception(academic_career, admit_term)
    except Exception as exc:
        print(f"None value error: {exc}")
        print(f"Values of the parameters: {exc.args[0]} {exc.args[1]}")
        return
    students_filter = generate_students_filter(student_list)
    table = "SYSADM.PS_CST_FIN_STD_VW" if discontinued is False else "SYSADM.PS_CST_FIN_STD_N_VW"
    sql = f"""SELECT EMPLID
        ,NAME
        ,INSTITUTION
        ,CAMPUS
        ,ACAD_CAREER
        ,ACAD_PROG
        ,ACAD_PLAN
        ,ADMIT_TERM
        ,NVL(NATIONAL_ID, '') AS NATIONAL_ID
        ,SEX
        ,NVL(CST_KSH, '') AS CST_KSH
        ,NVL(CST_ZKZH, '') AS CST_ZKZH
        ,PROG_STATUS
        ,SCC_ROW_ADD_DTTM
        ,SCC_ROW_UPD_DTTM FROM {table} 
        WHERE 1=1
        AND ACAD_CAREER = :academic_career 
        AND ADMIT_TERM = :admit_term 
        {students_filter}"""
    cursor = connection.cursor()
    cursor.execute(sql, academic_career=academic_career, admit_term=admit_term)
    columns = [col[0] for col in cursor.description]
    cursor.rowfactory = lambda *args: dict(zip(columns, args))  # fetch each row of a query as a dictionary
    rows = cursor.fetchall()
    students = []
    fee_year = data_transformation_utils.get_year(fee_term)
    for i, row in enumerate(rows):
        student = tuple((row["EMPLID"],
                         row["NAME"],
                         data_transformation_utils.getXXDM(row["INSTITUTION"]),
                         data_transformation_utils.getXYDM(row["CAMPUS"]),
                         data_transformation_utils.getZYDM(row["ACAD_CAREER"]),
                         data_transformation_utils.get_class_id(row["ADMIT_TERM"], row["ACAD_CAREER"],
                                                                row["ACAD_PROG"]),
                         row["NATIONAL_ID"],
                         data_transformation_utils.getJC(row["ADMIT_TERM"]),
                         fee_year,
                         data_transformation_utils.getRXNY(row["ADMIT_TERM"]),
                         data_transformation_utils.getXZ(row["ACAD_CAREER"]),
                         "",
                         data_transformation_utils.getXB(row["SEX"]),
                         str(row["CST_ZKZH"]).strip(),
                         data_transformation_utils.getXJ(row["PROG_STATUS"]),
                         data_transformation_utils.getXL(row["ACAD_CAREER"]),
                         "",
                         data_transformation_utils.getZDSX(row["PROG_STATUS"]),
                         data_transformation_utils.getZXZT(row["PROG_STATUS"])))
        students.append(",".join(student))
    write_file_content(f"data_files\\{fee_year}\\student\\{admit_term}\\student.txt", "w+", "GBK", "\n".join(students))


def create_fee_data_file(admit_term, fee_term, discontinued, student_list, fee_list, amounts):
    student_attribute_value = data_transformation_utils.get_year(admit_term)
    if discontinued:
        create_fee_data_file_discontinued(fee_term, student_list, fee_list, amounts)
        return
    students_filter = generate_students_filter(student_list)
    fees_filter = generate_fees_filter(fee_list)
    table = "SYSADM.PS_CST_FIN_FEE_VW"
    sql = f"""SELECT EMPLID
        ,NAME
        ,DESCR
        ,STRM
        ,DEFAULT_AMT 
        FROM {table} WHERE 1=1
        AND STDNT_ATTR = :stdnt_attr
        AND STDNT_ATTR_VALUE = :stdnt_attr_value 
        AND STRM = :strm
        {students_filter} {fees_filter}"""
    cursor = connection.cursor()
    cursor.execute(sql, stdnt_attr="YEAR", stdnt_attr_value=student_attribute_value, strm=fee_term)
    columns = [col[0] for col in cursor.description]
    cursor.rowfactory = lambda *args: dict(zip(columns, args))
    rows = cursor.fetchall()
    fee_items = []
    fee_year = data_transformation_utils.get_year(fee_term)
    for i, row in enumerate(rows):
        fee = tuple((row["EMPLID"],
                     row["NAME"],
                     row["DESCR"],
                     fee_year,
                     str(float(row["DEFAULT_AMT"]))))
        fee_items.append(",".join(fee))
    write_file_content(f"data_files\\{fee_year}\\fee\\{admit_term}\\fee.txt", "w+", "GBK", "\n".join(fee_items))


def create_fee_data_file_discontinued(fee_term, student_list, fee_list, amounts):
    students_filter = generate_students_filter(student_list, True)
    table = "ZJU.T_ZS_LQXX"
    sql = f"""SELECT LQBH,XM FROM {table} WHERE 1=1 {students_filter}"""
    cursor = connection.cursor()
    cursor.execute(sql)
    columns = [col[0] for col in cursor.description]
    cursor.rowfactory = lambda *args: dict(zip(columns, args))
    rows = cursor.fetchall()
    fees = {}
    fee_year = data_transformation_utils.get_year(fee_term)
    for row in rows:
        student_id = row["LQBH"]
        fees[student_id] = []
        student_info = [student_id, row["XM"]]
        for index, fee in enumerate(fee_list):
            item = [data_transformation_utils.get_fee_description(fee), fee_year,
                    amounts[index]]
            student = student_info.copy()
            student.extend(item)
            fees[student_id].append(student)
    fee_items = []
    for x, fee_collection in enumerate(fees.values()):  # List of fees for a student
        for y, fee in enumerate(fee_collection):  # Each fee item in the List
            fee_items.append(",".join(fee))
    write_file_content(f"data_files\\{fee_year}\\fee\\discontinued\\fee.txt", "w+", "GBK", "\n".join(fee_items))

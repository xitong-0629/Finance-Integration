import argparse


def get_parser():
    parser = argparse.ArgumentParser(prog="sf", description="Provide the financial information to the financial system")
    parser.add_argument("ft", help="set the fee term")
    parser.add_argument("at", nargs="+", help="set the admit term of the students")
    parser.add_argument("-c", "--career", dest="career", metavar="{Academic Career Code}", default="UGRD",
                        help="set the academic career of the students, (default: %(default)s)")
    parser.add_argument("-s", "--students", dest="students", metavar="{Student ID}", nargs="*",
                        help="filter by student ID")
    parser.add_argument("-f", "--fees", dest="fees", metavar="{Fees Type}", nargs="*",
                        choices=["102", "201", "301", "304"], help="filter by fees type")
    parser.add_argument("-a", "--amounts", dest="amounts", metavar="{Amounts}", nargs="*", help="fee amounts")
    parser.add_argument("-d", "--whether-discontinued", dest="discontinued",
                        action="store_true", help="whether the students are discontinued")
    parser.add_argument("-go", "--generate-organizations", dest="go",
                        action="store_true", help="whether to generate organizations data file")
    parser.add_argument("-gp", "--generate-plans", dest="gp",
                        action="store_true", help="whether to generate plans data file")
    parser.add_argument("-gc", "--generate-classes", dest="gc",
                        action="store_true", help="whether to generate classes data file")
    parser.add_argument("-gs", "--generate-students", dest="gs",
                        action="store_true", help="whether to generate students data file")
    parser.add_argument("-gf", "--generate-fees", dest="gf",
                        action="store_true", help="whether to generate fees data file")
    parser.add_argument("-uo", "--upload-organizations", dest="uo",
                        action="store_true", help="whether to upload organizations data file")
    parser.add_argument("-up", "--upload-plans", dest="up",
                        action="store_true", help="whether to upload plans data file")
    parser.add_argument("-uc", "--upload-classes", dest="uc",
                        action="store_true", help="whether to upload classes data file")
    parser.add_argument("-us", "--upload-students", dest="us",
                        action="store_true", help="whether to upload students data file")
    parser.add_argument("-uf", "--upload-fees", dest="uf",
                        action="store_true", help="whether to upload fees data file")
    parser.add_argument("-v", "--version", action="version", version="%(prog)s 1.0")
    return parser.parse_args()

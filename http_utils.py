# -*- coding: UTF-8 -*-
import os
import requests
from urllib3 import encode_multipart_formdata
from bs4 import BeautifulSoup
import data_transformation_utils

url = "https://pay.zju.edu.cn/wingsoft/msgService/service.jsp"
depart = "HNSF"
pwd = "hnsf201804"


def calculate_remark(htype, msgtype="", hord="", startym="", reccount="", amt=""):
    sum = 0
    plain_text_string = "&".join([htype, depart, pwd, msgtype, hord, startym, reccount, amt])
    plain_text_string_length = len(plain_text_string)
    for index in range(0, plain_text_string_length):
        index = index + 1
        sum = sum + (index * (index + 37) * (index + 52) * ord(plain_text_string[index - 1]))
    return sum


def get_file_lines(file_path):
    count = 0
    for index, line in enumerate(open(file_path, "r", encoding="GBK")):
        count += 1
    return count


def http_get_request_status(hord):
    htype = "001"  # 取单条上传型保报文信息
    payload = {
        "depart": depart,
        "pwd": pwd,
        "htype": htype,
        "hord": hord,
        "remark": calculate_remark(htype, "", hord, "", "", "")
    }
    (data, headers) = encode_multipart_formdata(payload)
    response = requests.post(url=url, headers={"Content-Type": headers}, data=data)
    print("===Http response status===")
    soup = BeautifulSoup(response.text, "html.parser")
    print(soup)
    try:
        msgtype = soup.msgtype.string
        startym = soup.startym.string
        reccount = soup.reccount.string
        amt = soup.amt.string
        htime = soup.htime.string
        state = soup.state.string
        result = soup.result.string
        print("hord:{0} - msgtype:{1} - startym:{2} - reccount:{3} - amt:{4} - "
              "htime:{5} - state:{6} - result:{7}"
              .format(str(hord), msgtype, startym, reccount, amt, htime, state, result))
    except AttributeError as err:
        print(err)
    print("==========================")


def http_get_lastord():
    htype = "000"
    remark = calculate_remark(htype, "", "", "", "", "")
    payload = {
        "depart": depart,
        "pwd": pwd,
        "htype": htype,
        "remark": remark
    }
    (data, headers) = encode_multipart_formdata(payload)
    response = requests.post(url=url, headers={"Content-Type": headers}, data=data)
    soup = BeautifulSoup(response.text, "html.parser")
    lastord = soup.lastord.string
    print("===Http get lastord response===")
    print(f"Response Encoding: {response.encoding}")
    print(f"Status Code: {str(response.status_code)}")
    print(f"Content Type: {response.headers['Content-Type']}")
    print(f"Date: {response.headers['Date']}")
    print(f"Lastord: {lastord}")
    print("===============================")
    return lastord


def get_hord():
    return str(int(http_get_lastord()) + 1)


def http_post_helper(msgtype, file_name, file_path):
    # 上传报文
    htype = "010"
    hord = get_hord()
    reccount = str(get_file_lines(file_path))

    amt = startym = ""
    if msgtype == "171":
        f = open(file_path, "r")
        amount = 0
        for line in f:
            amount += float(line.split(",")[-1])
        amt = str(amount)
        f.close()

    payload = {
        "depart": depart,
        "pwd": pwd,
        "htype": htype,
        "msgtype": msgtype,
        "hord": hord,
        "remark": calculate_remark(htype, msgtype, hord, startym, reccount, amt),
        "startym": startym,
        "reccount": reccount,
        "amt": amt,
        "msgtxt": (file_name, open(file_path, "rb").read())
    }

    (content, header) = encode_multipart_formdata(payload)
    response = requests.post(url=url, headers={"Content-Type": header}, data=content)
    http_get_request_status(hord)

    print("===Http response information===")
    print(f"Response Encoding: {response.encoding}")
    print(f"Status Code: {str(response.status_code)}")
    print(f"Content Type: {response.headers['Content-Type']}")
    print(f"Date: {response.headers['Date']}")
    print("===============================")


def traverse_directory(path):
    for root, subdirs, files in os.walk(path):
        for file in files:
            file_path = os.path.join(root, file)
            print(file_path)


def upload_classes(admit_term):
    msgtype = "133"  # 班级同步
    file_name = "class.txt"
    file_path = f"data_files\\class\\{admit_term}\\{file_name}"
    http_post_helper(msgtype, file_name, file_path)


def upload_students(fee_term, admit_term):
    msgtype = "103"  # 学生同步
    fee_year = data_transformation_utils.get_year(fee_term)
    file_name = "student.txt"
    file_path = f"data_files\\{fee_year}\\student\\{admit_term}\\{file_name}"
    http_post_helper(msgtype, file_name, file_path)


def upload_fees(fee_term, admit_term, discontinued):
    msgtype = "171"  # 学生收费信息同步
    fee_year = data_transformation_utils.get_year(fee_term)
    file_name = "fee.txt"
    file_path = f"data_files\\{fee_year}\\fee\\{admit_term}\\{file_name}"
    if discontinued:
        file_path = f"data_files\\{fee_year}\\fee\\discontinued\\{file_name}"
    http_post_helper(msgtype, file_name, file_path)


def upload_org():
    msgtype = "110"  # 学院增加
    file_name = "org.txt"
    file_path = "data_files\\org.txt"
    http_post_helper(msgtype, file_name, file_path)


def upload_plan():
    msgtype = "120"  # 专业增加
    file_name = "plan.txt"
    file_path = "data_files\\plan.txt"
    http_post_helper(msgtype, file_name, file_path)

# -*- coding: utf-8 -*-
def getXXDM(institution):
    content = "01" if institution == "ZJUNV" else ""
    return content


def getXYDM(campus):
    content = "29" if campus == "HN" else ""
    return content


def getZYDM(academic_career):
    content = "HNXQ" if academic_career == "UGRD" else "HNXQJH" if academic_career == "EXRD" else ""
    return content


def getXZ(academic_career):
    content = "4" if academic_career == "UGRD" else ""
    return content


def getXB(sex):
    content = "男" if sex == "M" else "女" if sex == "F" else "" if sex == "U" else ""
    return content


def getXL(academic_career):
    content = "本科" if academic_career == "UGRD" else ""
    return content


def getXJ(prog_status):
    content = "有" if prog_status == "AC" else "退学" if prog_status == "DC" else "保留学籍" if prog_status == "SP" else "休学" if prog_status == "LA" else "开除学籍" if prog_status == "DM" else "取消学籍" if prog_status == "CN" else "停学" if prog_status == "CM" else "其他"
    return content


def getZDSX(prog_status):
    content = "应届在读" if prog_status == "AC" else "退学" if prog_status == "DC" or prog_status == "DM" or prog_status == "CN" or prog_status == "CM" or prog_status == "SP" else "在读"
    return content


def getZXZT(prog_status):
    content = "在校" if prog_status == "AC" else "离校"
    return content


def get_year(strm):
    return "20" + strm[:2]


def getJC(strm):  #届次
    if get_year(strm) == "2016":
        return str(int(get_year(strm)) + 5)
    return str(int(get_year(strm)) + 4)


def getRXNY(strm):
    return get_year(strm) + "09"


def get_class_id(strm, academic_career, academic_program):
    if strm == "1610":
        return "2016HNBJ"
    elif strm == "1710":
        return "2017HNBJ"
    elif strm == "1810":
        return "2018HNBJ"
    elif strm == "1910":
        return "2019HNBJ"
    class_id = get_year(strm)
    class_id += "HNBJ" if academic_career == "UGRD" else "HNJH" if academic_career == "EXRD" else ""
    class_id += "I" if academic_program == "UC001" else "E" if academic_program == "UE001" else ""
    return class_id


def get_class_descr(strm, academic_career, academic_program):
    if strm == "1610":
        return "海宁国际校区2016级本科生"
    descr = "海宁国际校区"
    descr += get_year(strm)
    descr += "级"
    descr += "本科生" if academic_career == "UGRD" else "交换生" if academic_career == "EXRD" else ""
    descr += "-"
    descr += "ZJUI" if academic_program == "UC001" else "ZJE" if academic_program == "UE001" else ""
    return descr


def get_fee_description(fee):
    descr = ""
    if fee == "102":
        return "专业学费"
    elif fee == "201":
        return "住宿费"
    elif fee == "301":
        return "教材费"
    elif fee == "304":
        return "军训服装费"
    return descr

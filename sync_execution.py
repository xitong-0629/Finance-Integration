# -*- coding: utf-8 -*-
import data_file_generator
import argparse_utils
import http_utils


def execute():
    args = argparse_utils.get_parser()
    academic_career = args.career
    fee_term = args.ft
    admit_term_list = args.at
    discontinued = args.discontinued
    students = args.students
    fees = args.fees
    amounts = args.amounts
    go = args.go
    gp = args.gp
    gc = args.gc
    gs = args.gs
    gf = args.gf
    uo = args.uo
    up = args.up
    uc = args.uc
    us = args.us
    uf = args.uf

    student_list = []
    if students or students is not None:
        for student in students:
            student_list.append(student)

    fee_list = []
    if fees or fees is not None:
        for fee in fees:
            fee_list.append(fee)
    
    if amounts or amounts is not None:
        try:
            if fees is None:
                raise Exception(fees)
            elif len(fees) != len(amounts):
                raise Exception(f"Fees: {fees}", f"Amounts: {amounts}")
        except Exception as exc:
            print(f"Fees and amounts are not matched: {exc}")
            return
    else:
        try:
            if discontinued:
                raise Exception()
        except Exception as exc:
            print("Missing amounts for discontinued students")
            return

    print("Run Control Parameters")
    print("----------------------------------------")
    print(f"Academic Career: {academic_career}")
    print(f"Fee Term: {fee_term}")
    print(f"Admit Term: {admit_term_list}")
    print(f"Whether Discontinued: {discontinued}")
    print(f"Students Filter: {student_list}")
    print(f"Fees Filter: {fee_list}")
    print(f"Fee Amounts: {amounts}")
    print(f"Whether to generate organizations data file: {go}")
    print(f"Whether to generate plans data file: {gp}")
    print(f"Whether to generate classes data file: {gc}")
    print(f"Whether to generate students data file: {gs}")
    print(f"Whether to generate fees data file: {gf}")
    print(f"Whether to upload organizations data file: {uo}")
    print(f"Whether to upload plans data file: {up}")
    print(f"Whether to upload classes data file: {uc}")
    print(f"Whether to upload students data file: {us}")
    print(f"Whether to upload fees data file: {uf}")
    print("----------------------------------------")

    for admit_term in admit_term_list:
        print(f"Admit Term: {admit_term}")
        if go:
            data_file_generator.create_academic_org_data_file()
        if gp:
            data_file_generator.create_academic_plan_data_file()
        if gc:
            data_file_generator.create_class_data_file(admit_term, academic_career)
        if gs:
            data_file_generator.create_student_data_file(academic_career, admit_term, fee_term, discontinued, student_list)
        if gf:
            data_file_generator.create_fee_data_file(admit_term, fee_term, discontinued, student_list, fee_list, amounts)
        if uo:
            print(f"-----Organization upload execution start-----")
            http_utils.upload_org()
            print(f"-----Organization upload execution end-----")
        if up:
            print(f"-----Plan upload execution start-----")
            http_utils.upload_plan()
            print(f"-----Plan upload execution end-----")
        if uc:
            print(f"-----Class upload execution start-----")
            http_utils.upload_classes(admit_term)
            print(f"-----Class upload execution end-----")
        if us:
            print(f"-----Student upload execution start-----")
            http_utils.upload_students(fee_term, admit_term)
            print(f"-----Student upload execution end-----")
        if uf:
            print(f"-----Fee upload execution start-----")
            http_utils.upload_fees(fee_term, admit_term, discontinued)
            print(f"-----Fee upload execution end-----")


if __name__ == "__main__":
    execute()

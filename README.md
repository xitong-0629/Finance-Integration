README
===========================
将学生应收款数据推送到学校财务系统。
****
# 命令行参数
## Generate org & plan data files
2410 2410 -go -gp
## Generate class data files
2410 2010 2110 2210 2310 2410 -gc
## Generate student data files
2410 2110 2210 2310 -gs
## Generate student & fee data files
2310 1910 2010 2110 2210 2310 -gs -gf
## Generate student data files with filters
2410 1910 2010 2110 2210 2310 -gs -s 3220116162 3220116163 3220116164 3220116165 3220116166 3220116167 3220116168 3220116169 3220116170 3220116171
## Generate student & fee data files with filters
2310 1910 2010 2110 2210 2310 -gs -s 3220116162 3220116163 3220116164 3220116165 3220116166 3220116167 3220116168 3220116169 3220116170 3220116171 -gf -f 102 304
## Generate discontinued student & fee data files
2310 1910 2010 2110 2210 2310 -gs -s 3190110003 3190110004 3190110005 3190110087 3190110384 3210110012 3210110151 3210110410 3210110510 3210112457 -gf -f 102 201 -a 50 100 -d
# Data Source
## PS_CST_FIN_STD_VW
Normal Students<br>
1)Program status not in CM, DC, DM, CN<br>
2)Chinese students<br>
3)Not in PS_CST_STD_BDN<br>
## PS_CST_FIN_STD_N_VW
Discontinued students<br>
1)Program status in CM, DC, DM, CN<br>
2)Chinese students<br>
3)Not in PS_CST_STD_BDN